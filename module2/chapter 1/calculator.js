let t11 = document.getElementById("t11");
let t12 = document.getElementById("t12");
var t1 = document.getElementById("t1");
var t2 = document.getElementById("t2");
var t3 = document.getElementById("t3");
function sin1() {
    var a = Math.PI * parseFloat(t11.value) / 180;
    var b = Math.sin(a);
    t12.value = b.toString();
}
function cos1() {
    var c = Math.PI * parseFloat(t11.value) / 180;
    var d = Math.cos(c);
    t12.value = d.toString();
}
function tan1() {
    var e = Math.PI * parseFloat(t11.value) / 180;
    var f = Math.tan(e);
    t12.value = f.toString();
}
function sqrt() {
    var g = Math.sqrt(parseFloat(t11.value));
    t12.value = g.toString();
}
function add() {
    var c = parseFloat(t1.value) + parseFloat(t2.value);
    t3.value = c.toString();
}
function sub() {
    var c = parseFloat(t1.value) - parseFloat(t2.value);
    t3.value = c.toString();
}
function mult() {
    var c = parseFloat(t1.value) * parseFloat(t2.value);
    t3.value = c.toString();
}
function div() {
    var c = parseFloat(t1.value) / parseFloat(t2.value);
    t3.value = c.toString();
}
//# sourceMappingURL=calculator.js.map