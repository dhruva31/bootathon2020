function xyz() {
    let a = document.getElementById("x");
    let real = document.getElementById("real");
    let imaginary = document.getElementById("imaginary");
    var b = a.value;
    var c;
    var i;
    var j;
    i = b.indexOf("+");
    if (i == -1) {
        j = b.indexOf("-");
        var x = b.substring(0, j);
        var y = b.substring(j, b.length - 1);
        document.getElementById("real").innerHTML = "The real part=" + x;
        document.getElementById("imaginary").innerHTML = "The imaginary part=" + y;
    }
    else {
        var e = b.substring(0, i);
        var f = b.substring(i + 1, b.length - 1);
        document.getElementById("real").innerHTML = "The real part=" + e;
        document.getElementById("imaginary").innerHTML = "The imaginary part=" + parseFloat(f);
    }
}
//# sourceMappingURL=CN.js.map